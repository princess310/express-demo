<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<footer class="Bottomer">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-7 BottomLeft">
            	<div class="BottomLink">
                	<a href="/wzdt/index_38.aspx" style="padding-left: 0px;">网站地图</a><em>|</em>
                	<a href="/flsm/index_39.aspx">法律声明</a><em>|</em>
                	<a href="/yqlj/index_40.aspx">友情链接</a><em>|</em>
                	<a href="/lxwmVP/index_41.aspx">联系我们</a><em>|</em>
                	<a href="http://weibo.com/u/3295048395" target="_blank">微博关注</a>
                </div>
                <div class="Copyright">胡桃里音乐酒馆 © 2015 版权所有粤ICP备09119904号</div>
                <div class="Design">Designed by <a href="http://wanhu.com.cn" target="_blank">Wanhu</a> </div>
            </div>
            <div class="col-md-6 col-sm-5 BottomCont">
            	<div class="col-md-9 col-sm-7 col-xs-8 text-right">
                	<p>扫一扫，胡桃里微信：</p>
                    <div class="Bottom_Share">
                        <div class="bdsharebuttonbox ShareBox bds_tools bdshare-button-style2-16" data-bd-bind="1500621452743">
                            <a class="bds_tsina xlwb" title="分享到新浪微博" href="#" data-cmd="tsina"></a>
                            <a href="#" data-cmd="weixin" class="bds_weixin wx" title="分享到微信"></a>
                        </div>
                        <script type="text/javascript">
                            window._bd_share_config = { "common": { "bdSnsKey": {}, "bdText": "", "bdMini": "2", "bdMiniList": false, "bdPic": "", "bdStyle": "2", "bdSize": "16" }, "share": {} }; with (document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];
                        </script>
                    </div>
                </div>
            	<div class="col-md-3 col-sm-5 col-xs-4">
                    <img src="http://www.htaoli.com/uploadfiles/image/wechat.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
