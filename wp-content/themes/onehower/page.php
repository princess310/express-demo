<?php get_header(); ?>

    <div class="page-container">
        <?php if (have_posts()) : the_post(); update_post_caches($posts); ?>
            <?php the_content(); ?>
        <?php else : ?>
        <?php endif; ?>
    </div>

<?php get_footer(); ?>