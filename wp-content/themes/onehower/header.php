<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php if ( is_home() ) {
        bloginfo('name'); echo " - "; bloginfo('description');
    } elseif ( is_category() ) {
        single_cat_title(); echo " - "; bloginfo('name');
    } elseif (is_single() || is_page() ) {
        single_post_title();
    } elseif (is_search() ) {
        echo "搜索结果"; echo " - "; bloginfo('name');
    } elseif (is_404() ) {
        echo '页面未找到!';
    } else {
        wp_title('',true);
    } ?></title>
    <?php wp_head(); ?>
</head>

<?php flush(); ?>

<body>
    <header class="header-container">
        <div class="header-wrapper">
            <nav class="navbar navbar-default hzl-header">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="navbar-right">
                        <div class="share bdsharebuttonbox ShareBox bds_tools bdshare-button-style2-16" data-bd-bind="1500621452743">
                            <a class="bds_tsina xlwb" title="分享到新浪微博" href="#" data-cmd="tsina"></a>
                            <a href="#" data-cmd="weixin" class="bds_weixin wx" title="分享到微信"></a>
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/phone-call.png">
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li <?php if (is_home()) { echo 'class="current"';} ?>><a title="<?php bloginfo('name'); ?>"  href="<?php echo get_option('home'); ?>/">首页</a></li>
                                <?php wp_list_pages('depth=1&title_li=0&sort_column=menu_order'); ?>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
            <!-- Navigation Menu -->
        </div>
    </header>
