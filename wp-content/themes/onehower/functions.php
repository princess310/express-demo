<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/17 0017
 * Time: 上午 10:38
 */

/*
 * try to load scripts here
 */
function onehower_scripts() {
    // bootstrap stylesheet.
    wp_enqueue_style( 'onehower-bootstrp', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', array(), '3.7' );

    // Theme stylesheet.
    wp_enqueue_style( 'onehower-style', get_stylesheet_uri() );

    // Load bootstrap js
    wp_enqueue_script( 'onehower_bootstrap', get_theme_file_uri( '/bootstrap/js/bootstrap.min.js' ), array('jquery'), '1.0.0' );

    // Load main js
    wp_enqueue_script( 'main', get_theme_file_uri( '/assets/js/main.js' ), array(), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'onehower_scripts' );

// try to load other things when page loaded
function onehower_setup() {
}
add_action( 'after_setup_theme', 'onehower_setup' );


/*
 * Footer things
 */
function onehower_footer() {
}
add_action( 'wp_footer', 'onehower_footer' );