<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
  
  <section class="Contain">
    <div class="" id="dowebok">
        <div class="HomeBanner section" id="sectionIntroVideo" style="max-height: 520px; height: 520px;">
        <div class="TopHeight1" style="height: 0px;"></div>
            <div class="" id="HomeBan">
                <div class="BanList">
                <div class="tempWrap" style="overflow:hidden; position:relative;"><ul style="width: 3766px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 1000ms; transform: translate(-1883px, 0px) translateZ(0px);">
                    
                        <li class="slide" style="display: table-cell; vertical-align: top; width: 1883px;"><a href="" target="_blank"><img src="http://www.htaoli.com/uploadfiles/banner/IndexBan2.jpg" style="height: 520px;"></a></li>
                    
                        <li class="slide" style="display: table-cell; vertical-align: top; width: 1883px;"><a href="" target="_blank"><img src="http://www.htaoli.com/uploadfiles/banner/IndexBan1.jpg" style="height: 520px;"></a></li>
                    
                </ul></div>
                </div>
                <a href="javascript:void(0);" class="banNext"><img src="http://www.htaoli.com/cn/images/bg30_right.png"></a>
            </div>
            <div class="IndexNav"><img src="http://www.htaoli.com/cn/images/bgDH.gif"></div>
        </div>
        <div class="section Column1" id="sectionDesigner1">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav1.png">
            </div>
            <div class="ColCont1_1">
                
            	<a><img src="http://www.htaoli.com/uploadfiles/2015/11/20151127151706176.png"></a>
                <span><img src="http://www.htaoli.com/cn/images/bg32.png"></span>
            	<a><img src="http://www.htaoli.com/uploadfiles/2015/11/201511271517231723.png"></a>
                <span><img src="http://www.htaoli.com/cn/images/bg32.png"></span>
            	<a><img src="http://www.htaoli.com/uploadfiles/2015/11/201511271517361736.png"></a>
                
            </div>
            <div class="ColCont1_2">
            	<p>	超<span style="color:#804036;font-size:26px;">15</span>小时新型娱乐商业模式<br>营业时间从中午11:00到凌晨3:00提供从咖啡到美食正餐、红酒、洋酒、啤酒以及音乐演绎等</p>
            </div>
            <div class="ColCont1_3">
                <p style="margin-left:0px;">	<a href="#">首家胡桃里诞生于2013年深圳创意文化园以音乐碰撞美食美酒的融合为灵感而创建。</a></p><p>	<a href="#">温暖的就餐空间、质朴的美食态度、有品质的音乐表演，被消费者口碑评为“文青之家”。</a></p><p>	<a href="#">2014年8月底，胡桃里南宁店诞生，胡桃里品牌开启全国版图的扩张。[胡桃里]将吸引[一般不去酒吧消费的顾客],开始体验夜生活状态。</a></p>
                <div class="clear"></div>
            </div>
            <div class="ColCont1_4">
            	<ul>
                    
                        <li>
                    	    <a href="http://www.hutaolinight.com/htljcpd/list_23.aspx" target="_blank" class="ABottom">
                        	    <img src="http://www.htaoli.com/uploadfiles/image/indeximg8.jpg">
                            </a>
                            <a href="http://www.hutaolinight.com/htljcpd/list_23.aspx" target="_blank" class="ATop" style="background: rgba(103, 62, 56, 0.4);">
                                <img src="http://www.htaoli.com/uploadfiles/image/indeximg4.png" class="A" style="display: inline;">
                                <img src="http://www.htaoli.com/cn/images/indeximgmore.png" class="AHover" style="display: none;">
                            </a>
                        </li>
                    
                        <li>
                    	    <a href="http://www.hutaolinight.com/gypp/index_13.aspx" target="_blank" class="ABottom">
                        	    <img src="http://www.htaoli.com/uploadfiles/image/indeximg7.jpg">
                            </a>
                            <a href="http://www.hutaolinight.com/gypp/index_13.aspx" target="_blank" class="ATop" style="background: rgba(103, 62, 56, 0.4);">
                                <img src="http://www.htaoli.com/uploadfiles/image/indeximg3.png" class="A" style="display: inline;">
                                <img src="http://www.htaoli.com/cn/images/indeximgmore.png" class="AHover" style="display: none;">
                            </a>
                        </li>
                    
                        <li>
                    	    <a href="http://www.hutaolinight.com/htljk/index_18.aspx" target="_blank" class="ABottom">
                        	    <img src="http://www.htaoli.com/uploadfiles/image/indeximg6.jpg">
                            </a>
                            <a href="http://www.hutaolinight.com/htljk/index_18.aspx" target="_blank" class="ATop" style="background: rgba(103, 62, 56, 0.4);">
                                <img src="http://www.htaoli.com/uploadfiles/image/indeximg2.png" class="A" style="display: inline;">
                                <img src="http://www.htaoli.com/cn/images/indeximgmore.png" class="AHover" style="display: none;">
                            </a>
                        </li>
                    
                        <li>
                    	    <a href="http://www.hutaolinight.com/cxc/index_17.aspx" target="_blank" class="ABottom">
                        	    <img src="http://www.htaoli.com/uploadfiles/image/indeximg5.jpg">
                            </a>
                            <a href="http://www.hutaolinight.com/cxc/index_17.aspx" target="_blank" class="ATop">
                                <img src="http://www.htaoli.com/uploadfiles/image/indeximg1.png" class="A">
                                <img src="http://www.htaoli.com/cn/images/indeximgmore.png" class="AHover">
                            </a>
                        </li>
                    
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <div class="section Column2" id="sectionDesigner2">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav2.png">
                <div class="TimeL">
                	<a><img src="http://www.htaoli.com/cn/images/wucan.png"></a>
                	<a><img src="http://www.htaoli.com/cn/images/xiawucha.png"></a>
                	<a><img src="http://www.htaoli.com/cn/images/wancan.png"></a>
                	<a style="border-right: 0px;"><img src="http://www.htaoli.com/cn/images/yeshenghuo.png"></a>
                
                </div>
            </div>
            <div class="ColCont2_1">
                
            	    <a><img src="http://www.htaoli.com/uploadfiles/image/indeximg12.png" class="AShow1"><img src="http://www.htaoli.com/uploadfiles/image/indeximg15.png" class="AShow2"></a>
            	    <a><img src="http://www.htaoli.com/uploadfiles/image/indeximg13.png" class="AShow1" style="display: inline;"><img src="http://www.htaoli.com/uploadfiles/image/indeximg16.png" class="AShow2" style="display: none;"></a>
            	    <a><img src="http://www.htaoli.com/uploadfiles/image/indeximg14.png" class="AShow1" style="display: inline;"><img src="http://www.htaoli.com/uploadfiles/image/indeximg17.png" class="AShow2" style="display: none;"></a>
                
            </div>
            <div class="ColCont2_2">
            	<ul>
                    
                        
                            <li class="col-md-4 col-sm-4">
                    	        <a href="/htljk/index_18.aspx">
                        	        <div class="ShopImg">
                        		        <img class="Img1" src="http://www.htaoli.com/uploadfiles/image/img6.jpg">
                                        <div class="ImgTop">
                                	        <img src="http://www.htaoli.com/uploadfiles/image/wancan.png">
                                            <p>17:00~20:30</p>
                                        </div>
                                    </div>
                                    <div class="ShopCont">
                            	        <span>胡桃里酒库</span>
                                        <p>	以原生态食材为材料并融合中国甚至世界烹饪...</p>
                                        <em><img src="http://www.htaoli.com/cn/images/bg6.png"></em>
                                    </div>
                                </a>
                            </li>
                        
                    
                        
                            <li class="col-md-4 col-sm-4">
                    	        <a href="/btsg/index_19.aspx">
                        	        <div class="ShopImg">
                        		        <img class="Img1" src="http://www.htaoli.com/uploadfiles/2016/03/20160313125707577.jpg">
                                        <div class="ImgTop">
                                	        <img src="http://www.htaoli.com/uploadfiles/2016/03/20160313130024024.png">
                                            <p>17:00~02:00</p>
                                        </div>
                                    </div>
                                    <div class="ShopCont">
                            	        <span>吧台时光</span>
                                        <p></p>
                                        <em><img src="http://www.htaoli.com/cn/images/bg6.png"></em>
                                    </div>
                                </a>
                            </li>
                        
                    
                        
                            <li class="col-md-4 col-sm-4">
                    	        <a href="/cxc/index_17.aspx">
                        	        <div class="ShopImg">
                        		        <img class="Img1" src="http://www.htaoli.com/uploadfiles/image/img5.jpg">
                                        <div class="ImgTop">
                                	        <img src="http://www.htaoli.com/uploadfiles/image/xiawu.png">
                                            <p>14:00~17:00</p>
                                        </div>
                                    </div>
                                    <div class="ShopCont">
                            	        <span>餐饮</span>
                                        <p>	以原生态食材为材料并融合中国甚至世界烹饪...</p>
                                        <em><img src="http://www.htaoli.com/cn/images/bg6.png"></em>
                                    </div>
                                </a>
                            </li>
                        
                    
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <div class="section Column3" id="sectionDesigner3">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav3.png">
            </div>
            <div class="ColCont3_1">
            	
                	<img src="http://www.htaoli.com/uploadfiles/image/indeximg18.png"> <p style="color:#804036;font-weight:bold;">	音乐&amp;表演</p><p style="color:#6d5d52;">	清新民谣音乐现场，陪伴式音乐表演，就餐氛围更惬意；艺术唤醒视觉，国际艺术流派绽放胡桃里品赏舞台；</p><p style="color:#804036;font-weight:bold;">	它，让你感受不一样的娱乐氛围</p>
                
            </div>
            <div class="ColCont3_2">
            	<div class="DIV" id="DIV1">
                    <a href="javascript:void(0);" class="prev1"><img src="http://www.htaoli.com/cn/images/bg37.jpg"></a>
                    <a href="javascript:void(0);" class="next1"><img src="http://www.htaoli.com/cn/images/bg38.jpg"></a>
                	<div class="IndexImg1">
                        <div class="tempWrap" style="overflow:hidden; position:relative;"><ul style="width: 2048px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 0ms; transform: translate(-512px, 0px) translateZ(0px);">
                            
                                <li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://www.hutaolinight.com/zxhd/info_26.aspx?itemid=213" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/indeximg19.jpg"></a></li><li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://www.hutaolinight.com/zxhd/info_26.aspx?itemid=231" target="_blank"><img src="http://www.htaoli.com/uploadfiles/2015/12/201512311358125812.jpg"></a></li>
                            
                                <li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://www.hutaolinight.com/zxhd/info_26.aspx?itemid=213" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/indeximg19.jpg"></a></li>
                            
                        <li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://www.hutaolinight.com/zxhd/info_26.aspx?itemid=231" target="_blank"><img src="http://www.htaoli.com/uploadfiles/2015/12/201512311358125812.jpg"></a></li></ul></div>
                    </div>
                </div>
                <div class="DIV MusicBox" id="MusicBox">
                	<div class="Ti"><img src="http://www.htaoli.com/cn/images/Musice_ti.png" alt="熟悉的好歌"></div>
                    <div class="PlayerBox" id="PlayerBox">
                        
                        <div class="CD" id="CD">
                            <audio id="audio" class="audio" src="/uploadfiles/2015/10/201510311448394839.mp3"></audio>
                            <div class="CD_mark"></div>
                            <div class="CD_BG"></div>
                            <img src="http://www.htaoli.com/uploadfiles/2015/11/th_201511111749184918.jpg" alt="尼克">
                            <div class="CD_head"></div>
                        </div>
                        <div class="info">
                            <dl>
                                <dd class="Singer_Name" title="尼克">尼克</dd>
                                <dd class="Music_Name" title="歌名：You and me">歌名：<span>You and me</span></dd>
                                <dd class="Rhythm"><img src="http://www.htaoli.com/cn/images/music_Fluctuation.png"></dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                        
                     </div>
                    <div class="MusicList" id="MusicList">
                       <div class="aBtn Prev disable" title="已经没有上一个了"></div>
                       <div class="List" style="position: relative; overflow: hidden;">
                          <ul style="width: 192px; position: absolute; left: 0px; top: 0px;">
                             
                             <li mp3="/uploadfiles/2015/10/201510311448394839.mp3" songer="尼克" songname="You and me">
                                <figure title="尼克-You and me">
                                   <div class="pic">
                                      <div class="Mark"></div>
                                      <img src="http://www.htaoli.com/uploadfiles/2015/11/th_201511111749184918.jpg" alt="尼克">
                                   </div>
                                   <figcaption>尼克</figcaption>
                                </figure>
                             </li>
                             
                             <li mp3="/uploadfiles/2015/10/201510311441114111.mp3" songer="尼克" songname="Dida">
                                <figure title="尼克-Dida">
                                   <div class="pic">
                                      <div class="Mark"></div>
                                      <img src="http://www.htaoli.com/uploadfiles/2015/11/th_201511111749184918.jpg" alt="尼克">
                                   </div>
                                   <figcaption>尼克</figcaption>
                                </figure>
                             </li>
                             
                             <li mp3="/uploadfiles/2015/09/201509222049154915.mp3" songer="东格尔乐队" songname="思念">
                                <figure title="东格尔乐队-思念">
                                   <div class="pic">
                                      <div class="Mark"></div>
                                      <img src="http://www.htaoli.com/uploadfiles/2015/09/201509222048594859.png" alt="东格尔乐队">
                                   </div>
                                   <figcaption>东格尔乐队</figcaption>
                                </figure>
                             </li>
                             
                          </ul>
                       </div>
                       <div class="aBtn Next" title="下一个"></div>
                       <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="section Column4" id="sectionDesigner4">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav4.png">
            </div>
            <div class="ColCont4_1">
                
            	    <img src="http://www.htaoli.com/uploadfiles/image/indeximg24.png">
                
            </div>
            <div class="ColCont4_2">
            	
                	<p>	文化交流，轻奢主题时尚派对；原创艺术展览，假日欢乐庆典 ，文艺动物用心打造<br>现实融合艺趣，生活渗透理想，<br><span style="color:#804036;font-weight:bold;">相聚！是时光的珍藏。</span></p>
                
            </div>
            <div class="ColCont4_3">
            	<div class="carousel">
                    <ul class="slides" style="width: 1024px; height: 285px;">
                        
                            <div class="slideItem slide0" style="width: 320px; height: 289px; top: 0px; right: 352px; z-index: 6;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=230"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/201512252231453145.jpg" style="width: 320px; height: 189px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=230" class="title">方文山现身胡桃里竟是为了一...</a> <div id="divContent">	这几天，该是收礼物的时候了。是不是收完礼，觉得自己的幸福值比任...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide1" style="width: 256px; height: 251.2px; top: 37.8px; right: 208.64px; z-index: 5;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=229"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/201512221444504450.jpg" style="width: 256px; height: 151.2px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=229" class="title">中国连锁加盟品牌总评榜，胡...</a> <div id="divContent">										12月19-20日，中国加盟产业金鼎奖暨2015年中国连锁加盟...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide2" style="width: 204.8px; height: 220.96px; top: 68.04px; right: 93.952px; z-index: 4;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=228"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/2015123114030737.jpg" style="width: 204.8px; height: 120.96px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=228" class="title">音乐是唱出来的诗，胡桃里是...</a> <div id="divContent">	喜欢一首歌的时间，就在旋律出来的那几秒。爱上一个地方的时间，也...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide3" style="width: 163.84px; height: 196.768px; top: 92.232px; right: 2.2016px; z-index: 3;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=227"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/201512161521422142.jpg" style="width: 163.84px; height: 96.768px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=227" class="title">2016年的第一张民谣，春暖花...</a> <div id="divContent">	在民谣最好的时代，民谣遇见了胡桃里。	也让我们在这个充斥着电子...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide4" style="width: 163.84px; height: 196.768px; top: 92.232px; right: 857.958px; z-index: 3;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=226"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/20151214140034034.jpg" style="width: 163.84px; height: 96.768px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=226" class="title">食尚遇见时尚，胡桃里助力颜...</a> <div id="divContent">	12月10日，颜诺国际在深圳举行了时尚发布会，展示了5个新锐设计师...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide5" style="width: 204.8px; height: 220.96px; top: 68.04px; right: 725.248px; z-index: 4;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=225"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/201512122130483048.jpg" style="width: 204.8px; height: 120.96px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=225" class="title">风靡60个城市每10天开一个胡...</a> <div id="divContent">	2015年12月8日，星期二 合纵文化集团3楼会议室	深圳的天气微冷，合...<span></span></div>
                               </div></div>
                            </div>
                        
                            <div class="slideItem slide6" style="width: 256px; height: 251.2px; top: 37.8px; right: 559.36px; z-index: 5;">
                               <div class="itemBox">
                                   <a href="/htljcpd/info_23.aspx?itemid=223"><img alt="" src="http://www.htaoli.com/uploadfiles/2015/12/20151209150726726.jpg" style="width: 256px; height: 151.2px;"></a>
                               </div>
                               <div class="intro"><div class="cont">
                                   <a href="/htljcpd/info_23.aspx?itemid=223" class="title">Power of love，一场柔美与力...</a> <div id="divContent">		南宁胡桃里特邀来自乌克兰“爱的力量 Power of love”，力量与柔美...<span></span></div>
                               </div></div>
                            </div>
                        
                    <div class="spinner" style="display: none; width: 320px; height: 189px; top: 0px; right: 352px; opacity: 1; z-index: 10; position: absolute; cursor: pointer; overflow: hidden; padding: 0px; margin: 0px; border: none;"></div><div class="videoOverlay" style="display: none; width: 320px; height: 189px; top: 0px; right: 352px; opacity: 1; z-index: 9; position: absolute; cursor: pointer; overflow: hidden; padding: 0px; margin: 0px; border: none;"></div></ul>
                <div class="nextButton" style="right: 352px;"></div><div class="prevButton" style="left: 352px;"></div></div>
            </div>
        </div>
        <div class="section Column5" id="sectionDesigner5">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav5.png">
            </div>
            <div class="ColCont5_1">
            	
                	<img src="http://www.htaoli.com/uploadfiles/image/img51.png"><br><p>	胡桃里会员计划限量征集108颗“胡桃仁“。当月一次性充值3万元，或一年内累计达3万元，都有机会成为胡桃仁。<br><span style="color:#804036;font-weight:bold;">108名，满员即止。了解更多专享</span> </p>
                
            </div>
            <div class="ColCont5_2">
            	<div class="DIV" id="ColDIV1">
                    <a href="javascript:void(0);" class="prev1"><img src="http://www.htaoli.com/cn/images/bg37.jpg"></a>
                    <a href="javascript:void(0);" class="next1"><img src="http://www.htaoli.com/cn/images/bg38.jpg"></a>
                	<div class="IndexImg2">
                        <div class="tempWrap" style="overflow:hidden; position:relative;"><ul style="width: 1536px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 0ms; transform: translate(-512px, 0px) translateZ(0px);">
                            
                                <li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://baidu.com" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/indeximg26.jpg"></a></li><li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://baidu.com" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/indeximg26.jpg"></a></li>
                            
                        <li style="display: table-cell; vertical-align: top; width: 512px;"><a href="http://baidu.com" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/indeximg26.jpg"></a></li></ul></div>
                    </div>
                </div>
                <div class="DIV" id="ColDIV2" style="height: 204px;">
                	<div class="IndexNews">
                        <div class="TabList">
                            <ul>
                                <li><img src="http://www.htaoli.com/cn/images/tabimg1.png"></li>
                                <li><img src="http://www.htaoli.com/cn/images/tabimg2.png"></li>
                                <div class="clear"></div>
                            </ul>
                        </div>
                        <div class="IndexNewsList">
                        	<div class="NewsLei">
                            	<div class="NewsList1">
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList0">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                	<div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList1">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList2">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList3">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><div class="tempWrap" style="overflow:hidden; position:relative;"><ul style="width: 430px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 1000ms; transform: translate(0px, 0px) translateZ(0px);">
                                                    
                                                        <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                            <div class="NewsDay1">
                                                                <p><span>23/</span>2015-04</p>
                                                            </div>
                                                            <div class="NewsCont1">
                                                                <a href="/hyr/info_25.aspx?itemid=23">胡桃里诗友...</a>
                                                                <p>	人民网南宁3月30日电 （李天支）3月29日下午，胡桃里诗社...</p>
                                                                <a href="/hyr/info_25.aspx?itemid=23" class="more"><img src="http://www.htaoli.com/cn/images/bg6.png"></a>
                                                            </div>
                                                        </li>
                                                    
                                                </ul></div></div></div></div></div></div></div></div>
                                            </div>
                                        </div>
                                    </div>
                                	<div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList4">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList5">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList6">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                	<div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList7">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList8">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList9">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                	<div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList10">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="MonNewsList">
                                    	<div class="MonNewsList1" id="MonNewsList11">
                                            <a href="javascript:void(0);" class="next3"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                            <div class="MonNewsList2">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="MonList">
                                	<ul>
                                    	<li>1月</li>
                                    	<li>2月</li>
                                    	<li>3月</li>
                                    	<li>4月</li>
                                    	<li>5月</li>
                                    	<li>6月</li>
                                    	<li>7月</li>
                                    	<li>8月</li>
                                    	<li>9月</li>
                                    	<li>10月</li>
                                    	<li>11月</li>
                                    	<li>12月</li>
                                        <div class="clear"></div>
                                    </ul>
                                </div>
                            </div>
                        	<div class="NewsLei">
                            	<div class="Newest" id="Newest">
                                	<a href="javascript:void(0);" class="next4"><img src="http://www.htaoli.com/cn/images/next.png"></a>
                                    <div class="Newest1">
                                        <div class="tempWrap" style="overflow:hidden; position:relative;"><ul style="width: 2150px; position: relative; overflow: hidden; padding: 0px; margin: 0px; transition-duration: 1000ms; transform: translate(-860px, 0px) translateZ(0px);">
                                            
                                                <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                    <a href="/zxhd/info_26.aspx?itemid=231">
                                                        <span class="titleName">2015年的书都在胡桃里...</span>
                                                        <p>	翻开2015这本书，365页只有屈指可数的那几页...</p>
                                                        <img src="http://www.htaoli.com/cn/images/bg6.png"><span class="TimeSpan">2015-12-28</span>
                                                    </a>
                                                </li>
                                            
                                                <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                    <a href="/zxhd/info_26.aspx?itemid=224">
                                                        <span class="titleName">今日头条‘深圳教育行业...</span>
                                                        <p>	伴随着移动互联网时代的兴起，大数据被公认...</p>
                                                        <img src="http://www.htaoli.com/cn/images/bg6.png"><span class="TimeSpan">2015-12-09</span>
                                                    </a>
                                                </li>
                                            
                                                <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                    <a href="/zxhd/info_26.aspx?itemid=222">
                                                        <span class="titleName">20位国际顶尖设计大咖...</span>
                                                        <p>										国际设计空间大奖艾特奖“Idea-To...</p>
                                                        <img src="http://www.htaoli.com/cn/images/bg6.png"><span class="TimeSpan">2015-12-07</span>
                                                    </a>
                                                </li>
                                            
                                                <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                    <a href="/zxhd/info_26.aspx?itemid=220">
                                                        <span class="titleName">送一个DIY的圣诞给自己...</span>
                                                        <p>	下面这张图中列出了正宗圣诞必须要做的一些...</p>
                                                        <img src="http://www.htaoli.com/cn/images/bg6.png"><span class="TimeSpan">2015-12-04</span>
                                                    </a>
                                                </li>
                                            
                                                <li style="display: table-cell; vertical-align: top; width: 430px;">
                                                    <a href="/zxhd/info_26.aspx?itemid=218">
                                                        <span class="titleName">壹周刊：11月的故事，一...</span>
                                                        <p>	今天是11月的最后一天，也是一周开始的第一...</p>
                                                        <img src="http://www.htaoli.com/cn/images/bg6.png"><span class="TimeSpan">2015-12-02</span>
                                                    </a>
                                                </li>
                                            
                                        </ul></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="section Column6" id="sectionDesigner6">
        	<div class="TopHeight" style="height: 50px;"></div>
        	<div class="ColumnName">
            	<img src="http://www.htaoli.com/cn/images/bgnav6.png">
            </div>
            <div class="ColCont6_1">
            	
                	<img src="http://www.htaoli.com/uploadfiles/image/img52.png"><br>
                
            </div>
            <div class="ColCont6_2">
            	<ul>
                	<li class="col-md-4">
                    	<div class="Title1">
                            
                	            <img src="http://www.htaoli.com/uploadfiles/image/indeximg28.png">
                            
                        </div>
                    	<div class="Cont6_1">
                        
                	        <span>胡桃里商业模式优势</span> <p>	一站式融合超15小时商业新模式，午餐/下午茶/晚餐/夜生活。</p><br><span>胡桃里空间设计优势</span> <p>	怀旧质朴与现代设计创新集合,传统建筑形成式阁楼体验。</p><br><span>演绎优势</span> <p>	以唱为主的清表演形式，不仅不影响就餐体验，又增加就餐氛围。</p><br><span>集团采供优势</span> <p>	比市场整体价格更加低，实现利润最大化。</p><br><span>资源优势</span> <p>	提供全国统一的演员调配。</p><br><span>品牌传播优势</span> <p>	凤凰卫视采访胡桃里品牌运营者。<br>深圳卫视当红美食栏目《食客准备》专访胡桃里。<br>《美味百事通》2013明星餐厅评选，胡桃里获“十大概念餐饮名店”荣誉。</p><br>
                        
                        </div>
                    </li>
                    <li class="col-md-4">
                    	<div class="Title1 Title1_1">
                            
                	            <img src="http://www.htaoli.com/uploadfiles/image/indeximg29.png">
                            
                        </div>
                        <div class="Cont6_3">
                            
                	            <img src="http://www.htaoli.com/uploadfiles/image/map.png">
                            
                        </div>
                    </li>
                	<li class="col-md-4">
                    	<div class="Title1">
                            
                	            <img src="http://www.htaoli.com/uploadfiles/image/indeximg30.png">
                            
                        </div>
                    	<div class="Cont6_2">
                        	
                	            <span>加盟商资质</span> <p>	1.有餐饮或酒吧经营经验和长期经营意愿，熟悉拟开“胡桃里”当地市场，有一定的人脉关系；</p><p>	2.喜爱餐饮及音乐文化，有自己的核心管理团队负责日常经营和管理，胡桃里提供培训、检查、指导。</p><br><span>物业条件及选址</span> <a href="#">查看详细 &gt;&gt;</a><br><span>加盟支持</span> <a target="_blank" href="http://www.hutaolinight.com/wmdjmzc/index_34.aspx">查看详细 &gt;&gt;</a><br><span>加盟流程</span> <a target="_blank" href="http://www.hutaolinight.com/jmlc/index_36.aspx/jmlc/index_36.aspx">查看详细 &gt;&gt;</a><br><span>在线申请</span> <a href="#"></a><a href="http://www.hutaolinight.com/wmdjmzc/index_34.aspx" target="_blank"><img src="http://www.htaoli.com/uploadfiles/image/bg39.png" alt=""></a><br>
                            
                        </div>
                    </li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php get_footer();